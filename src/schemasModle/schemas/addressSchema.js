import { Schema } from "mongoose";

export let addressSchema = Schema(
  {
    label: {
      type: String,
      trim: true,
      required: [true, "label field is required"],
      // enum: ["temporary", "permanent"],
    },
    combineLocation: {
      type: String,
      trim: true,
      required: [true, "combineLocation field is required"],
    },
    country: {
      type: String,
      trim: true,
      required: [true, "country field is required"],
      default: "nepal",
    },
    province: {
      type: Number,
      trim: true,
      required: [true, "province field is required"],
    },
    municipality: {
      type: String,
      trim: true,
      required: [true, "municipality field is required"],
    },
    wardNo: {
      type: Number,
      required: [true, "wardNo is required"],
    },
    district: {
      type: String,
      trim: true,
      required: [true, "district field is required"],
    },
    exactLocation: {
      type: String,
      trim: true,
      required: [true, "exactLocation field is required"],
    },
  },

  {
    timestamps: true,
  }
);
