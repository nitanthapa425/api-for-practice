import { Schema } from "mongoose";

export let houseSchema = Schema(
  {
    address: {
      type: String,
      trim: true,
      required: [true, "address field is required"],
    },
    bedrooms: {
      type: Number,
      trim: true,
      required: [true, "bedrooms field is required"],
    },
    bathrooms: {
      type: Number,
      trim: true,
      required: [true, "bathrooms field is required"],
    },
    price: {
      type: Number,
      trim: true,
      required: [true, "price field is required"],
    },
    houseCreatedAt: {
      type: Date,
      default: Date.now(),
    },
    hasGarden: {
      type: Boolean,
      default: false,
    },
  },

  {
    timestamps: true,
  }
);
