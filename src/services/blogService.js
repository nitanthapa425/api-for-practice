import { Blog } from "../schemasModle/model.js";

export const createBlogService = async ({ data }) => Blog.create(data);
export const updateBlogService = async ({ id, data }) =>
  Blog.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });
export const readSpecificBlogService = async ({ id }) => Blog.findById(id);
export const readAllBlogService = async ({
  find = {},
  sort = "",
  limit = "",
  skip = "",
  select = "",
}) => Blog.find(find).sort(sort).limit(limit).skip(skip).select(select);
export const deleteSpecificBlogService = async ({ id }) =>
  Blog.findByIdAndDelete(id);
