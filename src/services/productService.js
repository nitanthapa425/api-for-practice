import { Product } from "../schemasModle/model.js";
// for find refrence take refrence from learnNodejs1
// let data = [
//   {
//     name: "nitan1",
//     quantity: 13,
//     price: 999,
//     featured: true,
//     productImage: "localhost:8000/1674703260585sharmila.jpg",
//     manufactureDate: "2022-1-24",
//     company: "apple",
//   },
//   {
//     name: "nitan2",
//     quantity: 13,
//     price: 999,
//     featured: true,
//     productImage: "localhost:8000/1674703260585sharmila.jpg",
//     manufactureDate: "2022-1-24",
//     company: "apple",
//   },
//   {
//     name: "nitan3",
//     quantity: 13,
//     price: 999,
//     featured: true,
//     productImage: "localhost:8000/1674703260585sharmila.jpg",
//     manufactureDate: "2022-1-24",
//     company: "apple",
//   },
// ];

export const createProductService = async ({ data }) => Product.create(data);
// for insertMany use insertMany methode here data is array of object
// export const createProductService = async () => Product.insertMany(data);

export const updateProductService = async ({ id, data }) =>
  Product.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });
export const readSpecificProductService = async ({ id }) =>
  Product.findById(id).populate("reviews");
export const readAllProductService = async ({
  find = {},
  sort = "",
  limit = "",
  skip = "",
  select = "",
}) =>
  Product.find(find)
    .sort(sort)
    .limit(limit)
    .skip(skip)
    .select(select)
    .populate("reviews");
export const deleteSpecificProductService = async ({ id }) =>
  Product.findByIdAndDelete(id);

// the below readAllProductService is just for practice
// export const readAllProductService = async ({
//   find = { price: { $lte: 100 } },
//   sort = "",
//   limit = "",
//   skip = "",
//   select = "",
// }) => {
//   // return Product.find({ price: { $gte: 100, $lte: 999 } });
//   // return Product.find({ price: { $gte: 100, $lte: 999 } });
//   // main comparator operatior are
//   // $eq $neq
//   // $gt $gte
//   // $lt $lte
//   // $in $nin
//   //$mod   is for reminder
//   //logical operator
//   // &and , &or &not &nor
//   // return Product.find({ price: { $gte: 100, $lte: 999 } });
//   // { price: {$exists:true} } it means give those document whose price exist
//   // return Product.find({ price: { $exists: true } });
//   // find those whose price which is even
//   //{ price: { $mod: [2, 0] } //find those price which when divide by 2 produce 0 reminder
//   // return Product.find({ price: { $mod: [2, 0] } });
// };
