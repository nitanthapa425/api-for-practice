import { Review } from "../schemasModle/model.js";

export const createReviewService = async ({ data }) => Review.create(data);
export const updateReviewService = async ({ id, data }) =>
  Review.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });
export const readSpecificReviewService = async ({ id }) =>
  Review.findById(id).populate("productId");
export const readAllReviewService = async ({
  find = {},
  sort = "",
  limit = "",
  skip = "",
  select = "",
}) =>
  Review.find(find)
    .sort(sort)
    .limit(limit)
    .skip(skip)
    .select(select)
    .populate("productId");

export const deleteSpecificReviewService = async ({ id }) =>
  Review.findByIdAndDelete(id);
