import { Contact } from "../schemasModle/model.js";

export const createContactService = async ({ data }) => Contact.create(data);
export const updateContactService = async ({ id, data }) =>
  Contact.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });
export const readSpecificContactService = async ({ id }) =>
  Contact.findById(id);
export const readAllContactService = async ({
  find = {},
  sort = "",
  limit = "",
  skip = "",
  select = "",
}) => Contact.find(find).sort(sort).limit(limit).skip(skip).select(select);
export const deleteSpecificContactService = async ({ id }) =>
  Contact.findByIdAndDelete(id);
