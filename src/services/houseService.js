import { House } from "../schemasModle/model.js";

export const createHouseService = async ({ data }) => House.create(data);
export const updateHouseService = async ({ id, data }) =>
  House.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });
export const readSpecificHouseService = async ({ id }) => House.findById(id);
export const readAllHouseService = async ({
  find = {},
  sort = "",
  limit = "",
  skip = "",
  select = "",
}) => House.find(find).sort(sort).limit(limit).skip(skip).select(select);
export const deleteSpecificHouseService = async ({ id }) =>
  House.findByIdAndDelete(id);
