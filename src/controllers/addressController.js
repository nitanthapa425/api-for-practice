import { HttpStatus } from "../constant/constant.js";
import successResponseData from "../helper/successResponseData.js";
import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
// import { Address } from "../schemasModle/model.js";
import { addressService } from "../services/index.js";

export let createAddress = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  body.combineLocation = `${body.municipality},${body.wardNo}`;
  let data = await addressService.createAddressService({ data: body });//

  successResponseData({//
    res,
    message: "Address created successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});
export let updateAddress = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  body.combineLocation = `${body.municipality},${body.wardNo}`;
  let id = req.params.id;

  let data = await addressService.updateAddressService({ data: body, id });

  successResponseData({
    res,
    message: "Address updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let readSpecificAddress = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;

  let data = await addressService.readSpecificAddressService({ id });

  successResponseData({
    res,
    message: "Read  address successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let readAllAddress = tryCatchWrapper(async (req, res, next) => {
  let find = {};
  req.find = find;
  req.service = addressService.readAllAddressService;

  next();
});

export let deleteSpecificAddress = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let data = await addressService.deleteSpecificAddressService({ id });

  successResponseData({
    res,
    message: "Address delete successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});
