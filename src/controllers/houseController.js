import { HttpStatus } from "../constant/constant.js";
import successResponseData from "../helper/successResponseData.js";
import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
import { houseService } from "../services/index.js";

export let createHouse = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  console.log("**************", body);
  let data = await houseService.createHouseService({ data: body });

  successResponseData({
    res,
    message: "House created successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});
export let updateHouse = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let id = req.params.id;

  let data = await houseService.updateHouseService({ data: body, id });

  successResponseData({
    res,
    message: "House updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let readSpecificHouse = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;

  let data = await houseService.readSpecificHouseService({ id });

  successResponseData({
    res,
    message: "Read  house successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let readAllHouse = tryCatchWrapper(async (req, res, next) => {
  let find = {};
  req.find = find;
  req.service = houseService.readAllHouseService;

  next();
});

export let deleteSpecificHouse = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let data = await houseService.deleteSpecificHouseService({ id });

  successResponseData({
    res,
    message: "House delete successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});
