import { HttpStatus } from "../constant/constant.js";
import successResponseData from "../helper/successResponseData.js";
import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
import { productService } from "../services/index.js";

export let createProduct = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let data = await productService.createProductService({ data: body });

  successResponseData({
    res,
    message: "Product created successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});
export let updateProduct = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let id = req.params.id;

  let data = await productService.updateProductService({ data: body, id });

  successResponseData({
    res,
    message: "Product updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let readSpecificProduct = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;

  let data = await productService.readSpecificProductService({ id });

  successResponseData({
    res,
    message: "Read  product successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let readAllProduct = tryCatchWrapper(async (req, res, next) => {
  // for searching

  // &price=1234&quantity=5,6,7&role=admin,superadmin,hari,

  let find = {};
  if (req.query.name) {
    find.name = { $regex: req.query.name, $options: "i" };
  }

  if (req.query.price) {
    find.price = req.query.price;
  }

  if (req.query.quantity) {
    find.quantity = req.query.quantity;
  }

  req.find = find;
  req.service = productService.readAllProductService;
  next();
});

export let deleteSpecificProduct = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let data = await productService.deleteSpecificProductService({ id });

  successResponseData({
    res,
    message: "Product delete successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

// import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
// import { Product } from "../schemasModle/model.js";
// import { productService } from "../services/index.js";

// export let createProduct = tryCatchWrapper(async (req, res) => {
//   let data = { ...req.body };
//   let result = await productService.createProductService({ data });

//   let successJson = {
//     status: "success",
//     message: "Product created successfully.",
//     result: result,
//   };

//   res.status(201).json(successJson);
// });

// export let updateProduct = tryCatchWrapper(async (req, res) => {
//   let body = { ...req.body };
//   let id = req.params.id;

//   let result = await productService.updateProductService({ data: body, id });

//   let successJson = {
//     status: "success",
//     message: "Product updated successfully.",
//     result: result,
//   };

//   res.status(201).json(successJson);
// });

// export let readSpecificProduct = tryCatchWrapper(async (req, res) => {
//   let id = req.params.id;
//   let result = await Product.findById(id);
//   let successJson = {
//     status: "success",
//     message: "Read  product successfully.",
//     data: result,
//   };

//   res.status(200).json(successJson);
// });

// // export let readAllProduct = async (req, res) => {
// //   try {
// //     // for sorting
// //     let sort = sortingFun(req.query._sort);
// //     //for pagination
// //     let { limit, skip } = getExactPageData(
// //       req.query._brake,
// //       req.query._page,
// //       req.query._showAllData
// //     );

// //     let select = selectField(req.query._select);

// //     //for searching
// //     let find = {};

// //     if (req.query.name) {
// //       find.name = { $regex: req.query.name, $options: "i" };
// //     }

// //     if (req.query.price) {
// //       find.price = req.query.price;
// //     }

// //     if (req.query.quantity) {
// //       find.quantity = req.query.quantity;
// //     }

// //     let results = await Product.find(find)
// //       .sort(sort)
// //       .limit(limit)
// //       .skip(skip)
// //       .select(select);
// //     console.log("****", results);
// //     let totalDataInAPage = results.length;
// //     let totalResults = await Product.find({});
// //     let totalDataInWholePage = totalResults.length;
// //     let currentPage = req.query._page || 1;

// //     let successJson = {
// //       status: "success",
// //       message: "Read all product successfully.",
// //       data: {
// //         results,
// //         totalDataInAPage,
// //         totalDataInWholePage,
// //         currentPage,
// //       },
// //     };

// //     res.status(200).json(successJson);
// //   } catch (error) {
// //     let errorJson = {
// //       status: "failure",
// //       message: error.message,
// //     };

// //     let newStatus = error.statusCode || 400;
// //     res.status(newStatus).json(errorJson);
// //   }
// // };

// export let readAllProduct = tryCatchWrapper(async (req, res, next) => {
//   // for searching

//   // &price=1234&quantity=5,6,7&role=admin,superadmin,hari,

//   let find = {};
//   if (req.query.name) {
//     find.name = { $regex: req.query.name, $options: "i" };
//   }

//   if (req.query.price) {
//     find.price = req.query.price;
//   }

//   if (req.query.quantity) {
//     find.quantity = req.query.quantity;
//   }

//   req.find = find;
//   req.service = productService.readAllProductService;
//   next();
// });

// export let deleteSpecificProduct = tryCatchWrapper(async (req, res) => {
//   let id = req.params.id;
//   let result = await Product.findByIdAndDelete(id);

//   let successJson = {
//     status: "success",
//     message: "Product delete successfully.",
//     data: result,
//   };

//   res.status(200).json(successJson);
// });
