import { HttpStatus } from "../constant/constant.js";
import successResponseData from "../helper/successResponseData.js";
import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
import { contactService } from "../services/index.js";

export let createContact = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let data = await contactService.createContactService({ data: body });

  successResponseData({
    res,
    message: "Contact created successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});
export let updateContact = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let id = req.params.id;

  let data = await contactService.updateContactService({ data: body, id });

  successResponseData({
    res,
    message: "Contact updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let readSpecificContact = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;

  let data = await contactService.readSpecificContactService({ id });

  successResponseData({
    res,
    message: "Read  contact successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let readAllContact = tryCatchWrapper(async (req, res, next) => {
  let find = {};
  req.find = find;
  req.service = contactService.readAllContactService;

  next();
});

export let deleteSpecificContact = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let data = await contactService.deleteSpecificContactService({ id });

  successResponseData({
    res,
    message: "Contact delete successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});
