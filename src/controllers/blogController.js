import { HttpStatus } from "../constant/constant.js";
import successResponseData from "../helper/successResponseData.js";
import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
import { blogService } from "../services/index.js";

export let createBlog = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let data = await blogService.createBlogService({ data: body });

  successResponseData({
    res,
    message: "Blog created successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});
export let updateBlog = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let id = req.params.id;

  let data = await blogService.updateBlogService({ data: body, id });

  successResponseData({
    res,
    message: "Blog updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let readSpecificBlog = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;

  let data = await blogService.readSpecificBlogService({ id });

  successResponseData({
    res,
    message: "Read  blog successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let readAllBlog = tryCatchWrapper(async (req, res, next) => {
  let find = {};
  req.find = find;
  req.service = blogService.readAllBlogService;

  next();
});

export let deleteSpecificBlog = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let data = await blogService.deleteSpecificBlogService({ id });

  successResponseData({
    res,
    message: "Blog delete successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});
