import { Router } from "express";
import { houseController } from "../controllers/index.js";
import { sortFilterPagination } from "../middleware/sortSelectPage.js";
import upload from "../middleware/uploadFile.js";
export const houseRouter = Router();

houseRouter
  .route("/")
  // dont pass image it is design such that
  .post(houseController.createHouse)
  .get(houseController.readAllHouse, sortFilterPagination);
houseRouter
  .route("/:id")
  .patch(houseController.updateHouse)
  .get(houseController.readSpecificHouse)
  .delete(houseController.deleteSpecificHouse);

export default houseRouter;
