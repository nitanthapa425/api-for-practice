//review create (only customer(but we use admin only))
//review read() (no login required)
//review read specific (no login required)
//review update(only admin and superadmin)
//review delete (only admin and superadmin)

import { Router } from "express";
import { reviewController } from "../controllers/index.js";

import { isAuthorized } from "../middleware/isAuthorized.js";
import { isValidToken } from "../middleware/isValidToken.js";
import { sortFilterPagination } from "../middleware/sortSelectPage.js";

export const reviewRouter = Router();

reviewRouter
  .route("/")
  .post(
    // isValidToken({isTokenInDatabase:true}),
    // isAuthorized(["admin", "superAdmin"]),
    reviewController.createReview
  )
  .get(reviewController.readAllReview, sortFilterPagination)
  // .get(readAllReview)
  .delete();
reviewRouter
  .route("/:id")

  .patch(
    // isValidToken({isTokenInDatabase:true}),
    // isAuthorized(["admin", "superAdmin"]),
    reviewController.updateReview
  )
  .get(reviewController.readSpecificReview)
  .delete(
    // isValidToken({isTokenInDatabase:true}),
    // isAuthorized(["admin", "superAdmin"]),
    reviewController.deleteSpecificReview
  );

export default reviewRouter;
